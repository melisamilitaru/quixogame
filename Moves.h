#pragma once
#include "GameBoard.h"


class Moves
{
public:
	Moves();

	void moveLeft(GameBoard::Position& position, GameBoard& gameBoard, Piece::Side &piece);
	void moveUp(GameBoard::Position& position, GameBoard& gameBoard, Piece::Side &piece);
	void moveDown(GameBoard::Position& position, GameBoard& gameBoard, Piece::Side &piece);
	void moveRight(GameBoard::Position& position, GameBoard& gameBoard, Piece::Side &piece);
};