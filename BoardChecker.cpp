#include "BoardChecker.h"

BoardChecker::State BoardChecker::CheckGameBoard(const GameBoard & gameBoard)
{
	for (int indexLine = 0; indexLine < 5; indexLine++)
		for (int indexColumn = 0; indexColumn < 5; indexColumn++)
			if (gameBoard.board[indexLine][indexColumn] == gameBoard.board[indexLine][indexColumn + 1] == 'X' &&
				gameBoard.board[indexLine][indexColumn + 1] == gameBoard.board[indexLine][indexColumn + 2] == 'X')
				return State::WonFirstPlayer;
			else if (gameBoard.board[indexLine][indexColumn] == gameBoard.board[indexLine][indexColumn + 1] == '0' &&
				gameBoard.board[indexLine][indexColumn + 1] == gameBoard.board[indexLine][indexColumn + 2] == '0')
				return State::WonSecondPlayer;

	for (int indexLine = 0; indexLine < 5; indexLine++)
		for (int indexColumn = 0; indexColumn < 5; indexColumn++)
			if (gameBoard.board[indexLine][indexColumn] == gameBoard.board[indexLine + 1][indexColumn] == 'X' &&
				gameBoard.board[indexLine + 1][indexColumn] == gameBoard.board[indexLine + 2][indexColumn] == 'X')
				return State::WonFirstPlayer;
			else if (gameBoard.board[indexLine][indexColumn] == gameBoard.board[indexLine + 1][indexColumn] == '0' &&
				gameBoard.board[indexLine + 1][indexColumn] == gameBoard.board[indexLine + 2][indexColumn] == '0')
				return State::WonSecondPlayer;

	if (gameBoard.board[0][0] == gameBoard.board[1][1] == gameBoard.board[2][2] == gameBoard.board[3][3] == gameBoard.board[4][4] == 'X' ||
		gameBoard.board[0][4] == gameBoard.board[1][3] == gameBoard.board[2][2] == gameBoard.board[3][1] == gameBoard.board[4][0] == 'X')
		return State::WonFirstPlayer;
	else if (gameBoard.board[0][0] == gameBoard.board[1][1] == gameBoard.board[2][2] == gameBoard.board[3][3] == gameBoard.board[4][4] == '0' ||
		gameBoard.board[0][4] == gameBoard.board[1][3] == gameBoard.board[2][2] == gameBoard.board[3][1] == gameBoard.board[4][0] == '0')
		return State::WonSecondPlayer;

	return State::None;
}