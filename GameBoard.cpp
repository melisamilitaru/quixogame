#include "GameBoard.h"
#include <iostream>
#include <optional>

const char kEmptyBoardCell[] = "__";

void GameBoard::showGameboard()
{
	for (int i = 0; i < 5; i++)
	{
		for (int j = 0; j < 5; j++)
		{
			if (board[i][j] == 'X' || board[i][j] == '0') 
				std::cout << board[i][j];
			else 
			{
				std::cout << kEmptyBoardCell << " ";
			}
		}
		std::cout << std::endl;
	}
}

/*const std::optional<Piece>& GameBoard::operator[](const Position& pos)
{
	const auto&[line, column] = pos;
	return m_pieces[line * numberOfLines + column];
}
*/

void GameBoard::operator=(const GameBoard & board)
{
}

/*std::ostream & operator<<(std::ostream & os, const GameBoard& board)
{
	for (auto line = 0; line < GameBoard::numberOfLines; ++line)
	{
		for (auto column = 0; column < GameBoard::numberOfColumns; ++column)
		{
			if (board[line][column])
				os << board[line][column];
			else
				os << kEmptyBoardCell;
		}
		os << std::endl;
	}
	return os;
}
*/