#include "Moves.h"
#include "GameBoard.h"
#include <iostream>

Moves::Moves()
{
}

void Moves::moveDown(GameBoard::Position& position, GameBoard& gameBoard, Piece::Side &piece)
{
	if (position.first == 4)
	{
		std::cout << "You have to choose another option because the Piece is already down.";
		Moves moves;
		int option;
		std::cout << "Your option must be: \n  1(move up) \n  2(move to the right) \n  3(move to the left) \nPlease enter your option:";
		std::cin >> option;

		switch (option) {
		case 1:
			std::cout << "You chose to put the Piece up." << std::endl;
			moves.moveUp(position, gameBoard, piece);
			break;
		case 2:
			std::cout << "You chose to put the Piece on the right." << std::endl;
			moves.moveRight(position, gameBoard, piece);
			break;
		case 3:
			std::cout << "You chose to put the Piece on the left." << std::endl;
			moves.moveLeft(position, gameBoard, piece);
			break;
		default:
			break;
		}
	}

	if (piece == Piece::Side::sideX) {
		if (gameBoard.board[position.first][position.second] != '0')
		{
			int i = 0;
			while (position.first + i < 4)
			{
				gameBoard.board[position.first + i][position.second]
					= gameBoard.board[position.first + i + 1][position.second];
				i++;
			}
			gameBoard.board[4][position.second] = 'X';
		}
	}
	else if (piece == Piece::Side::sideO)
	{
		if (gameBoard.board[position.first][position.second] != 'X')
		{
			int i = 0;
			while (position.first + i < 4)
			{
				gameBoard.board[position.first + i][position.second] = gameBoard.board[position.first + i + 1][position.second];
				i++;
			}
			gameBoard.board[4][position.second] = '0';
		}
		std::cout << std::endl;
		gameBoard.showGameboard();
		std::cout << std::endl;
	}
}

void Moves::moveUp(GameBoard::Position& position, GameBoard& gameBoard, Piece::Side &piece)
{
	if (position.first == 0)
	{
		std::cout << "You have to choose another option because the Piece is already up.";
		Moves moves;
		int option;
		std::cout << "Your option must be: \n  1(move down) \n  2(move to the right) \n  3(move to the left) \nPlease enter your option:";
		std::cin >> option;

		switch (option) {
		case 1:
			std::cout << "You chose to put the Piece down." << std::endl;
			moves.moveDown(position, gameBoard, piece);
			break;
		case 2:
			std::cout << "You chose to put the Piece on the right." << std::endl;
			moves.moveRight(position, gameBoard, piece);
			break;
		case 3:
			std::cout << "You chose to put the Piece on the left." << std::endl;
			moves.moveLeft(position, gameBoard, piece);
			break;
		default:
			break;
		}
	}
	if (piece == Piece::Side::sideX) {
		if (gameBoard.board[position.first][position.second] != '0')
		{
			int i = 0;
			while (position.first - i >= 0)
			{
				gameBoard.board[position.first - i][position.second] = gameBoard.board[position.first - i - 1][position.second];
				i++;
			}
			gameBoard.board[0][position.second] = 'X';
		}
	}
	else if (piece == Piece::Side::sideO) {
		if (gameBoard.board[position.first][position.second] != 'X')
		{
			int i = 0;
			while (position.first - i >= 0)
			{
				gameBoard.board[position.first - i][position.second] = gameBoard.board[position.first - i - 1][position.second];
				i++;
			}
			gameBoard.board[0][position.second] = '0';
		}
	}


	std::cout << std::endl;
	gameBoard.showGameboard();
	std::cout << std::endl;
}

void Moves::moveLeft(GameBoard::Position& position, GameBoard& gameBoard, Piece::Side &piece)
{
	if (position.second == 0)
	{
		std::cout << "You have to choose another option because the Piece is already on the left.";
		Moves moves;
		int option;
		std::cout << "Your option must be: \n  1(move up) \n  2(move down) \n  3(move to the right) \nPlease enter your option:";
		std::cin >> option;

		switch (option) {
		case 1:
			std::cout << "You chose to put the Piece up." << std::endl;
			moves.moveUp(position, gameBoard, piece);
			break;
		case 2:
			std::cout << "You chose to put the Piece down." << std::endl;
			moves.moveDown(position, gameBoard, piece);
			break;
		case 3:
			std::cout << "You chose to put the Piece on the right." << std::endl;
			moves.moveRight(position, gameBoard, piece);
			break;
		default:
			break;
		}
	}

	if (piece == Piece::Side::sideX) {
		if (gameBoard.board[position.first][position.second] != '0')
		{
			int i = 0;
			while (position.second - i >= 0)
			{
				gameBoard.board[position.first][position.second - i] = gameBoard.board[position.first][position.second - i - 1];
				i++;
			}
			gameBoard.board[position.first][0] = 'X';
		}
	}
	else if (piece == Piece::Side::sideO) {
		if (gameBoard.board[position.first][position.second] != 'X')
		{
			int i = 0;
			while (position.second - i >= 0)
			{
				gameBoard.board[position.first][position.second - i] = gameBoard.board[position.first][position.second - i - 1];
				i++;
			}
			gameBoard.board[position.first][0] = '0';
		}
	}
	std::cout << std::endl;
	gameBoard.showGameboard();
	std::cout << std::endl;
}

void Moves::moveRight(GameBoard::Position& position, GameBoard& gameBoard, Piece::Side &piece)
{
	if (position.second == 4)
	{
		std::cout << "You have to choose another option because the Piece is already on the right.";
		Moves moves;
		int option;
		std::cout << "Your option must be: \n  1(move up) \n  2(move down) \n  3(move to the left) \nPlease enter your option:";
		std::cin >> option;

		switch (option) {
		case 1:
			std::cout << "You chose to put the Piece up." << std::endl;
			moves.moveUp(position, gameBoard, piece);
			break;
		case 2:
			std::cout << "You chose to put the Piece down." << std::endl;
			moves.moveDown(position, gameBoard, piece);
			break;
		case 3:
			std::cout << "You chose to put the Piece on the left." << std::endl;
			moves.moveLeft(position, gameBoard, piece);
			break;
		default:
			break;
		}
	}

	if (piece == Piece::Side::sideX)
	{
		if (gameBoard.board[position.first][position.second] != '0')
		{
			int i = 0;
			while (position.second + i < 4)
			{
				gameBoard.board[position.first][position.second + i] = gameBoard.board[position.first][position.second + i + 1];
				i++;
			}
			gameBoard.board[position.first][4] = 'X';
		}
	}
	else if (piece == Piece::Side::sideO) {
		if (gameBoard.board[position.first][position.second] != 'X')
		{
			int i = 0;
			while (position.second + i < 4)
			{
				gameBoard.board[position.first][position.second + i] = gameBoard.board[position.first][position.second + i + 1];
				i++;
			}
			gameBoard.board[position.first][4] = '0';
		}
	}
	std::cout << std::endl;
	gameBoard.showGameboard();
	std::cout << std::endl;
}