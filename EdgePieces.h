#pragma once
#include "Piece.h"
#include "GameBoard.h"
#include <vector>
#include <iostream>

class EdgePieces
{
public:
	EdgePieces();
	GameBoard::Position ChoosePosition();
	bool check(int coordX, int coordY, std::vector<std::pair<int, int>> vectorOfPositions);
};