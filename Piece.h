#pragma once
#include <iostream>

class Piece
{
public:
	enum class Side : char
	{
		None = '_',
		sideX = 'X',
		sideO = '0'
	};

public:
	Piece();
	Piece(Side side);
	//Piece(const Piece &other);
	//Piece(Piece && other);
	//Piece& Piece::operator=(const Piece&);
	~Piece();

	Side GetSide(Side side) const;

	friend std::ostream& operator << (std::ostream& os, const Piece& piece);
	friend std::istream& operator >> (std::istream& is, const Piece& piece);

private:
	Side m_side;
};