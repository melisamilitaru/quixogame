#pragma once
#include "GameBoard.h"
#include "EdgePieces.h"
#include <iostream>

class Player 
{
public:
	Player(const std::string& name);
	GameBoard PutPieceOnBoard(Piece::Side& piece, GameBoard& gameBoard, EdgePieces& EdgePieces) const;

	friend std::ostream& operator << (std::ostream& os, const Player& player);
public:
	std::string m_name;
};