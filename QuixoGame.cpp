#include "QuixoGame.h"
#include "Player.h"
#include "GameBoard.h"
#include "BoardChecker.h"
#include"Piece.h"
#include "EdgePieces.h"
#include <string>


void QuixoGama::Run()
{
	std::cout << "     Q U I X O    G A M E"  << std::endl<<std::endl;
	std::cout << "  Player1 (X) - Player2 (O)" << std::endl;
	
	std::string playerName;

	std::cout << "Please enter first player name: ";
	std::cin >> playerName;
	Player firstPlayer(playerName);

	std::cout << "Please enter second player name: ";
	std::cin >> playerName;
	Player secondPlayer(playerName);
	
	GameBoard board;
	EdgePieces edgePieces;

	while (true)
	{
		system("cls");

		std::cout << "The board looks like this: " << std::endl;
		board.showGameboard();

		std::cout << "\nPlayers have to choose a Position from the followings: \n 00, 01, 02, 03, 04, 10, 14, 20, 24, 30, 34, 40, 41, 42, 43, 44!\n"<<std::endl;
		std::cout << "Please choose a position from the margin of the game board." << std::endl;
		//edgePieces.ChoosePosition();
		
		Piece::Side sideFirstPlayer = Piece::Side::sideX;
		Piece::Side sideSecondPlayer = Piece::Side::sideO;
		bool ok = true;

		while (true)
		{
			if (ok == true) {
				firstPlayer.PutPieceOnBoard(sideFirstPlayer, board, edgePieces);
				ok = false;
			}
			else
			{
				secondPlayer.PutPieceOnBoard(sideSecondPlayer, board, edgePieces);
				ok = true;
			}
			
			auto state = BoardChecker::CheckGameBoard(board);
			if (state == BoardChecker::State::WonFirstPlayer) {
				std::cout << firstPlayer << " is the winner! Congratulations!";
				break;
			}
			else if (state == BoardChecker::State::WonSecondPlayer)
			{
				std::cout << secondPlayer << " is the winner! Congratulations!";
				break;
			}

			std::swap(firstPlayer, secondPlayer);
		}
	}
}